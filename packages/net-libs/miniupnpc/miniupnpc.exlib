# Copyright 2010 Pierre Lejeune <superheron@gmail.com>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Should still work with py2 but nothing needs it in our repos
require setup-py [ blacklist=2 import=setuptools multibuild=false with_opt=true ]

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="UPnP client library and a simple UPnP client"
DESCRIPTION="
The MiniUPnP project is a library and a daemon. The library is aimed to enable applications to use
the capabilities of a UPnP Internet Gateway Device present on the network to forward ports. The
daemon adds the UPnP Internet Gateway Device functionality to a NAT gateway running
OpenBSD/NetBSD/FreeBSD/Solaris with PF/IPF or Linux 2.4.x/2.6.x with netfilter. One of its most
interesting features is to enforce some permissions to allow or deny redirections, bringing some
security to UPnP. Newer versions also support the NAT-PMP protocol from Apple.
"
HOMEPAGE="https://miniupnp.tuxfamily.org"
if ! ever is_scm; then
    DOWNLOADS="${HOMEPAGE}/files/${PNV}.tar.gz"
fi

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

miniupnpc_src_prepare() {
    edo sed \
        -e "s#^INSTALLDIRMAN =.*#INSTALLDIRMAN = /usr/share/man#" \
        -i Makefile

    if option python ; then
        setup-py_src_prepare
    else
        default
    fi
}

miniupnpc_src_configure() {
    default

    optionq python && setup-py_src_configure
}

miniupnpc_src_compile() {
    default

    optionq python && setup-py_src_compile
}

miniupnpc_src_test() {
    emake "${DEFAULT_SRC_TEST_PARAMS[@]}" HAVE_IPV6="yes" check
}

miniupnpc_src_install() {
    emake DESTDIR="${IMAGE}" INSTALLPREFIX="/usr/$(exhost --target)" install

    optionq python && setup-py_src_install
}

