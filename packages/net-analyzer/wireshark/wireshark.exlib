# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake freedesktop-desktop freedesktop-mime gtk-icon-cache \
    option-renames [ renames=[ 'qt5 gui' ] ]

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="network protocol analyzer, formerly known as Ethereal"
DESCRIPTION="
Wireshark (formerly Ethereal) is a network protocol analyzer, or packet sniffer,
that lets you capture and interactively browse the contents of network frames.
The goal of the project is to create a commercial-quality packet analyzer for
Unix, and the most useful packet analyzer on any platform.
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/src/all-versions/${PN}-${PV/-}.tar.xz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    bcg729    [[ description = [ Enable support for G.729 codec in the RTP player ] ]]
    brotli    [[ description = [ Brotli compression format support ] ]]
    caps
    doc
    geoip     [[ description = [ Enable support for GeoIP location data ] ]]
    gnutls
    gui       [[ description = [ Build the Qt GUI and the rtp_player (uses qtmultimedia) ] ]]
    ilbc      [[ description = [ Support for iLBC codec in RTP player ] ]]
    journald
    kerberos
    lua       [[ description = [ Enable support for LUA scripting ] ]]
    lz4       [[ description = [ LZ4 lossless compression algorithm is used in some protocol (CQL...) ] ]]
    opus      [[ description = [ Support for opus codec in RTP player ] ]]
    snappy    [[ description = [ Snappy decompression in CQL and Kafka dissectors ] ]]
    smi       [[ description = [ Use libsmi to resolve numeric OIDs into human readable format ] ]]
    zstd
    gui? ( ( providers: qt5 qt6 ) [[ number-selected = exactly-one ]] )
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

# Network stuff
RESTRICT="test"

QT5_MIN_VER=5.12.0

DEPENDENCIES="
    build:
        app-text/asciidoctor[>=1.5]
        dev-lang/python:*[>=3.6]
        sys-devel/flex
        sys-devel/gettext
        virtual/pkg-config[>=0.15.0]
        doc? (
            app-doc/doxygen
            app-text/docbook-xml-dtd:4.5
            app-text/docbook-xsl-stylesheets
            dev-libs/libxslt [[ note = [ xsltproc ] ]]
        )
        gui? (
            providers:qt5? ( x11-libs/qttools:5[>=${QT5_MIN_VER}] )
            providers:qt6? ( x11-libs/qttools:6 )
        )
    build+run:
        dev-libs/glib:2[>=2.50.0]
        dev-libs/libgcrypt[>=1.8.0]
        dev-libs/libgpg-error
        dev-libs/libpcap[>=0.8]
        dev-libs/libxml2:2.0
        dev-libs/pcre2
        dev-perl/Parse-Yapp [[ note = [ HTTP header TPG plugin ] ]]
        group/${PN}
        media-libs/speexdsp [[ note = [ sharkd and wireshark ] ]]
        net-dns/c-ares[>=1.13.0]
        net-libs/libnl:3.0
        net-libs/libssh[>=0.6]
        net-libs/nghttp2[>=1.11.0]
        sys-libs/zlib
        bcg729? ( media-sound/bcg729 )
        brotli? ( app-arch/brotli )
        caps? ( sys-libs/libcap )
        geoip? ( net-libs/libmaxminddb )
        gnutls? ( dev-libs/gnutls[>=3.5.8] )
        gui? (
            providers:qt5? (
                x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui]
                x11-libs/qtmultimedia:5[>=${QT5_MIN_VER}]
            )
            providers:qt6? (
                x11-libs/qt5compat:6
                x11-libs/qtbase:6[gui]
                x11-libs/qtmultimedia:6
            )
        )
        ilbc? ( media-libs/ilbc )
        journald? ( sys-apps/systemd )
        lz4? ( app-arch/lz4 )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        lua? ( dev-lang/lua:=[>=5.1&<5.3] )
        opus? ( media-libs/opus )
        smi? ( net-libs/libsmi )
        snappy? ( app-arch/snappy )
        zstd? ( app-arch/zstd[>=1.0.0] )
    run:
        gui? (
            providers:qt5? ( x11-libs/qtsvg:5[>=${QT5_MIN_VER}] )
            providers:qt6? ( x11-libs/qtsvg:6 )
        )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCMAKE_INSTALL_DATADIR:PATH=/usr/share/wireshark
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DBUILD_androiddump:BOOL=TRUE
    -DBUILD_capinfos:BOOL=TRUE
    -DBUILD_captype:BOOL=TRUE
    -DBUILD_ciscodump:BOOL=TRUE
    -DBUILD_dftest:BOOL=TRUE
    -DBUILD_dpauxmon:BOOL=TRUE
    -DBUILD_etwdump:BOOL=FALSE
    -DBUILD_logray:BOOL=FALSE
    -DBUILD_randpkt{,dump}:BOOL=TRUE
    -DBUILD_sharkd:BOOL=TRUE
    -DBUILD_sshdump:BOOL=TRUE
    -DBUILD_text2pcap:BOOL=TRUE
    -DBUILD_{dump,edit,merge,reorder}cap:BOOL=TRUE
    -DBUILD_{raw,tf,t}shark:BOOL=TRUE
    -DBUILD_udpdump:BOOL=TRUE
    -DBUILD_wifidump:BOOL=TRUE
    -DDUMPCAP_INSTALL_GROUP:STRING=wireshark
    -DDUMPCAP_INSTALL_OPTION:STRING=normal
    -DENABLE_ASAN:BOOL=FALSE
    -DENABLE_ASSERT:BOOL=FALSE
    -DENABLE_AIRPCAP:BOOL=FALSE
    -DENABLE_APPLICATION_BUNDLE:BOOL=FALSE
    -DENABLE_CCACHE:BOOL=FALSE
    -DENABLE_CHECKHF_CONFLICT:BOOL=FALSE
    -DENABLE_COMPILER_COLOR_DIAGNOSTICS:BOOL=TRUE
    -DENABLE_DEBUG:BOOL=FALSE
    -DENABLE_DUMPCAP_GROUP:BOOL=TRUE
    -DENABLE_EXTRA_COMPILER_WARNINGS:BOOL=FALSE
    -DENABLE_FUZZER:BOOL=FALSE
    -DENABLE_LIBXML2:BOOL=TRUE
    -DENABLE_MINIZIP:BOOL=TRUE
    -DENABLE_NETLINK:BOOL=TRUE
    -DENABLE_PCAP:BOOL=TRUE
    -DENABLE_PLUGIN_IFDEMO:BOOL=FALSE
    -DENABLE_PLUGINS:BOOL=TRUE
    -DENABLE_SBC:BOOL=FALSE
    -DENABLE_SINSP:BOOL=FALSE
    -DENABLE_SPANDSP:BOOL=FALSE
    -DENABLE_STATIC:BOOL=FALSE
    -DENABLE_UBSAN:BOOL=FALSE
    -DENABLE_WERROR:BOOL=FALSE
    -DENABLE_WINSPARKLE:BOOL=FALSE
    -DENABLE_ZLIB:BOOL=TRUE
    -DUSE_STATIC:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    "bcg729 BCG729"
    "brotli BROTLI"
    "caps CAP"
    "gnutls GNUTLS"
    "ilbc ILBC"
    "kerberos KERBEROS"
    "lua LUA"
    "lz4 LZ4"
    "opus OPUS"
    "smi SMI"
    "snappy SNAPPY"
    "zstd ZSTD"
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    "geoip mmdbresolve"
    "gui wireshark"
    "journald sdjournal"
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    "doc DOXYGEN"
    "doc XSLTPROC"
)
CMAKE_SRC_CONFIGURE_OPTION_USES=(
    "providers:qt6 qt6"
)

wireshark_src_install() {
    cmake_src_install

    edo chown root:wireshark "${IMAGE}"/usr/$(exhost --target)/bin/dumpcap
    edo chmod 0750 "${IMAGE}"/usr/$(exhost --target)/bin/dumpcap

    if ! option caps ; then
        # install setuid
        edo chmod 6750 "${IMAGE}"/usr/$(exhost --target)/bin/dumpcap
    fi

    insinto /usr/$(exhost --target)/include/wiretap
    doins "${CMAKE_SOURCE}"/wiretap/wtap.h

    dodoc \
        "${CMAKE_SOURCE}"/doc/randpkt.txt \
        "${FILES}"/README.Exherbo
}

wireshark_pkg_postinst() {
    if option caps ; then
        # set capabilities
        edo setcap cap_net_raw,cap_net_admin+ep /usr/$(exhost --target)/bin/dumpcap
    fi

    if option gui ; then
        freedesktop-desktop_pkg_postinst
        freedesktop-mime_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

wireshark_pkg_postrm() {
    if option gui ; then
        freedesktop-desktop_pkg_postrm
        freedesktop-mime_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

