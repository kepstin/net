# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=grafana tag=v${PV} ] \
    flag-o-matic \
    systemd-service [ systemd_files=[ tools/packaging/loki.service ] ]

SUMMARY="Like Prometheus, but for logs"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: need to figure out
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.19]
    build+run:
        group/loki
        user/loki
"

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_unpack() {
    default

    edo pushd "${WORK}"
    esandbox disable_net
    edo go mod download -x
    esandbox enable_net
    edo popd
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/grafana/loki
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/grafana/loki

    edo ln -s "${TEMP}"/go/pkg "${WORKBASE}"/build

    edo sed \
        -e 's:/tmp/loki/rules:/etc/loki/rules:g' \
        -e 's:/tmp/loki:/var/lib/loki:g' \
        -e 's:#analytics:analytics:g' \
        -e 's:#  reporting_enabled:  reporting_enabled:g' \
        -i cmd/loki/loki-local-config.yaml
}

src_compile() {
    for binary in loki loki-canary logcli; do
        GO111MODULE="auto" edo go build \
            -o bin/${binary} \
            -ldflags "${LDFLAGS} -X github.com/grafana/loki/pkg/build.Version=${PV}" \
            -v \
            "./cmd/${binary}"
    done
}

src_install() {
    dobin bin/{loki,loki-canary,logcli}

    insinto /etc/${PN}
    newins cmd/loki/loki-local-config.yaml config.yml
    keepdir /etc/${PN}/rules

    install_systemd_files

    keepdir /var/lib/${PN}
    edo chown -R loki:loki "${IMAGE}"/var/lib/${PN}
}

pkg_postinst() {
    # FIXME: We can't use keepdir above as the rules directory has
    # to be empty and/or contain only valid rules data.
    # https://github.com/grafana/loki/issues/4859
    if [[ ! -d "${ROOT}/etc/loki/rules/fake" ]]; then
        edo mkdir -p "${ROOT}/etc/loki/rules/fake"
    fi
}

