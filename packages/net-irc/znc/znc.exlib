# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Purpose License v2

if ever is_scm ; then
    SCM_REPOSITORY="git://github.com/znc/znc.git"
    SCM_SECONDARY_REPOSITORIES="csocket googletest"
    SCM_EXTERNAL_REFS="third_party/Csocket:csocket third_party/googletest:googletest"
    SCM_csocket_REPOSITORY="https://github.com/jimloco/Csocket"
    SCM_googletest_REPOSITORY="https://github.com/google/googletest"
    require scm-git
else
    DOWNLOADS="https://${PN}.in/releases/archive/${PNV}.tar.gz"
fi

GTEST_VER=1.8.0

require cmake
# python stuff isn't installed in python version specific dirs, so no easy
# way to multibuild.
require python [ blacklist=2 multibuild=false with_opt=true ]
require 66-service systemd-service [ systemd_files=[ ] ]

export_exlib_phases src_fetch_extra src_prepare src_configure src_compile \
                    src_test src_install

SUMMARY="Advanced IRC Bouncer / Proxy"
DESCRIPTION="
ZNC is an IRC bounce/proxy with many advanced features like detaching, multiple
users, multiple clients per user, channel/query playback buffers, SSL, IPv6,
transparent DCC bouncing, C++/Perl module support, party line, and Web
administration.
"
HOMEPAGE="https://${PN}.in"
UPSTREAM_CHANGELOG="https://wiki.${PN}.in/ChangeLog/${PV} [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="http://en.${PN}.in/wiki/ZNC"
LICENCES="Apache-2.0"
SLOT="0"

MYOPTIONS="
    nls [[ description = [ Enable support for i18n internationalization  ] ]]
    perl [[ description = [ Enable Perl-based extension modules ] ]]
    python [[ description = [ Enable Python3-based extension modules ] ]]
    sasl [[ description = [ Enable SASL authentication support ] ]]
    tcl [[ description = [ Enable TCL-based extension modules ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        nls? (
            sys-devel/gettext
        )
        virtual/pkg-config
    build+run:
        nls? (
            dev-libs/boost
        )
        dev-libs/icu:=
        user/znc
        perl? (
            dev-lang/perl:*[>=5.10]
            dev-lang/swig[>=3.0.0]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        python? ( dev-lang/swig[>=3.0.0] )
        sasl? ( net-libs/cyrus-sasl )
        tcl? ( dev-lang/tcl )
"

znc_src_configure() {
    local cmakeparams=(
        -DSYSTEMD_DIR:PATH="${SYSTEMDSYSTEMUNITDIR}"
        -DWANT_ICU:BOOL=TRUE
        -DWANT_IPV6:BOOL=TRUE
        -DWANT_OPENSSL:BOOL=TRUE
        -DWANT_PYTHON_VERSION:STRING="python-$(python_get_abi)"
        # "Install znc.service to systemd"
        -DWANT_SYSTEMD:BOOL=TRUE
        -DWANT_ZLIB:BOOL=TRUE
        $(cmake_option nls WANT_I18N)
        $(cmake_option perl WANT_PERL)
        $(cmake_option python WANT_PYTHON)
        $(cmake_option sasl WANT_CYRUS)
        $(cmake_option tcl WANT_TCL)
    )

    if option perl || option python ; then
        cmakeparams+=( -DWANT_SWIG:BOOL=TRUE )
    else
        cmakeparams+=( -DWANT_SWIG:BOOL=FALSE )
    fi

    ecmake "${cmakeparams[@]}"
}

znc_src_fetch_extra() {
    if ever is_scm ; then
        scm_src_fetch_extra
    else
        if expecting_tests && [[ ! -e "${FETCHEDDIR}"/release-${GTEST_VER}.tar.gz ]]; then
            edo wget -P "${FETCHEDDIR}" https://github.com/google/googletest/archive/release-${GTEST_VER}.tar.gz
        fi
    fi
}

znc_src_prepare() {
    cmake_src_prepare

    if ! ever is_scm && expecting_tests ; then
        edo tar xvzf "${FETCHEDDIR}"/release-${GTEST_VER}.tar.gz -C third_party
        edo mv third_party/googletest-release-${GTEST_VER}/* third_party/googletest
    fi
}

znc_src_compile() {
    default

    expecting_tests && emake unittest_bin
}

znc_src_test() {
    emake unittest
}

znc_src_install() {
    default

    # ZNC needs this to save user configuration in case of being started
    # as a systemd service
    dodir /var/lib/znc
    keepdir /var/lib/znc
    edo chown znc:znc "${IMAGE}"/var/lib/znc

    install_66_files
}

